# dnVue

***A .NET programmer was developed, a front-end framework based on Vue3 and VITE!***

![dnVue](assets/Icons/App.png)

## Git Repositories

|**Repository**|
|---------|
|[https://github.com/niacomsoft/dnVue (Github)](https://github.com/niacomsoft/dnVue)|
|[https://jihulab.com/niacomsoft/dnvue (极狐 Gitlab Mirror)](https://jihulab.com/niacomsoft/dnvue)|
|[https://dev.azure.com/niacomsoft/dnVue (Microsoft Azure Mirror)](https://dev.azure.com/niacomsoft/dnVue)|
|[https://gitlab.com/n3824/dnvue (Gitlab Mirror)](https://gitlab.com/n3824/dnvue)|
|[https://codeup.aliyun.com/niacom/dnVue (阿里云云效 Mirror)](https://codeup.aliyun.com/niacom/dnVue)|

## Development Environment

|**Development Environment**|
|---------|
|Microsoft Windows 10 Professional Edition|
|[Microsoft VSCode](https://code.visualstudio.com/)|
|Node 16.14.0|
|Npm & Yarn|

## Technology Stack

- **Summary**
  - Vue 3
  - Vite
  - TypeScript
  - Vue-Router
  - Pinia (Instead of Vuex)
  - ElementPlus or Quasar

## [License](LICENSE.md)

>THE MIT LICENSE
>
> COPYRIGHT © 2022 WANG YUCAI.
> 
> PERMISSION IS HEREBY GRANTED, FREE OF CHARGE, TO ANY PERSON OBTAINING A COPY OF THIS > SOFTWARE AND ASSOCIATED DOCUMENTATION FILES (THE "SOFTWARE"), TO DEAL IN THE SOFTWARE > WITHOUT RESTRICTION, INCLUDING WITHOUT LIMITATION THE RIGHTS TO USE, COPY, MODIFY, MERGE, > PUBLISH, DISTRIBUTE, SUBLICENSE, AND/OR SELL COPIES OF THE SOFTWARE, AND TO PERMIT PERSONS > TO WHOM THE SOFTWARE IS FURNISHED TO DO SO, SUBJECT TO THE FOLLOWING CONDITIONS:
> 
> THE ABOVE COPYRIGHT NOTICE AND THIS PERMISSION NOTICE SHALL BE INCLUDED IN ALL COPIES OR > SUBSTANTIAL PORTIONS OF THE SOFTWARE.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, > INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR > PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE > FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR > OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER > DEALINGS IN THE SOFTWARE.